.. role:: white
.. role:: white-bold
.. role:: orange
.. role:: orange-bold
.. role:: black
.. role:: black-bold
.. role:: red
.. role:: red-bold
.. role:: small-font


========
Open Vfx
========

.. figure:: /_static/images/giants.jpg
   :class: fill

:white-bold:`'Standing on the shoulders of giants'`

.. note::
    * Introduction
    ** Open Source History
    ** Vfx History
    ** Open Source Adoption


Open Source History
====================

:orange-bold:`Early '80's`

.. figure:: /_static/images/bkg.png
   :class: fill

.. rst-class:: build

    * :white-bold:`1970 Universities`
    * :white-bold:`1983 GNU manifest`
        .. figure:: /_static/images/people/stallman.jpg
            :align: right
            :scale: 40 %

    * :white-bold:`1985 copy left`

:orange-bold:`'90's`
--------------------

.. figure:: /_static/images/bkg.png
   :class: fill

.. rst-class:: build

    * :white-bold:`1991 First release of Linux`
        .. figure:: /_static/images/people/torvald.jpg
            :align: right
            :scale: 100 %

    * :white-bold:`1991 Python`
    * :white-bold:`1993 Linux goes GPL`
    * :white-bold:`1993 RedHat`
    * :white-bold:`1995 MySql`
    * :white-bold:`1996 Apache`
    * :white-bold:`1998 Mozilla`
    * :white-bold:`1998 Open Source Initiative`
    * :white-bold:`2004 Canonical`

:orange-bold:`Nowadays`
-----------------------

.. figure:: /_static/images/bkg.png
   :class: fill


.. rst-class:: build

    :white-bold:`Linux is everywhere.`

    * :white-bold:`Internet !`
    * :white-bold:`Mobiles`
    * :white-bold:`Tv`
    * :white-bold:`Automotive`
    * :white-bold:`Fridges`
    * :white-bold:`Washing machines`


History of Vfx
==============

.. figure:: /_static/images/bkg2.png
   :class: fill

:orange-bold:`Early Years`

.. rst-class:: build

    *  :black-bold:`1896 : Georges Méliès`
        .. figure:: /_static/images/movies/dallaTerraAllaLuna.jpg
           :class: fill

    * :black-bold:`...`
    * :black-bold:`1977 : Star Wars`
        .. figure:: /_static/images/movies/starwars.png
           :class: fill

    * :black-bold:`1982 : Tron`
        .. figure:: /_static/images/movies/tron.jpg
           :class: fill

    * :black-bold:`1984 : The Adventures Of André And Wally B`
        .. figure:: /_static/images/movies/adventure.jpg
           :class: fill

    * :black-bold:`1985 : Young sherlock holmes`
        .. figure:: /_static/images/movies/yung.jpg
           :class: fill

    * :black-bold:`1989 : The Abyss`
        .. figure:: /_static/images/movies/abyss.jpeg
           :class: fill

:orange-bold:`'90's`
--------------------

.. figure:: /_static/images/bkg2.png
   :class: fill

.. rst-class:: build

    * :black-bold:`1991 : Terminator 2`
        .. figure:: /_static/images/movies/t2.jpg
           :class: fill

    * :black-bold:`1993 : Jurassic Park`
        .. figure:: /_static/images/movies/jp.jpg
           :class: fill

    * :black-bold:`1995 : Toy Story`
        .. figure:: /_static/images/movies/toy.jpg
           :class: fill

    * :black-bold:`1997 : Star Ship Troopers`
        .. figure:: /_static/images/movies/star.jpg
           :class: fill

    * :black-bold:`1998 : Gerys' game`
        .. figure:: /_static/images/movies/gary.png
           :class: fill

    * :black-bold:`1999 : Fight Club`
        .. figure:: /_static/images/movies/fight.jpg
           :class: fill

    * :black-bold:`1999 : Matrix`
        .. figure:: /_static/images/movies/matrix.jpg
           :class: fill


:orange-bold:`2000`
-------------------

.. figure:: /_static/images/bkg2.png
   :class: fill

.. rst-class:: build

    * :black-bold:`2002 : Lord Of the Rings`
        .. figure:: /_static/images/movies/lotr.png
           :class: fill

    * :black-bold:`2009 : Avatar`
        .. figure:: /_static/images/movies/avatar.jpg
           :class: fill

    * :black-bold:`...`
    * :black-bold:`2012: Avengers`
        .. figure:: /_static/images/movies/avengers.jpg
           :class: fill

    * :black-bold:`2015 : Interstellar`
        .. figure:: /_static/images/movies/interstellar.jpg
           :class: fill


Industry Adoption
=================


.. figure:: /_static/images/bkg3.png
   :class: fill


.. rst-class:: build

    * :white-bold:`Before 2000`
        .. figure:: /_static/images/sgi.jpg
            :class: fill

    * :white-bold:`1998 Maya`
        .. figure:: /_static/images/software/maya.png
            :align: right
            :scale: 30%

    * :white-bold:`2000 Maya On Linux`
        .. figure:: /_static/images/tux.png
            :align: right
            :scale: 50%

    * :white-bold:`~2001 Nvidia on linux`
        .. figure:: /_static/images/nvidia.png
            :align: right
            :scale: 50%

    * :white-bold:`2006 SGI stop producing Hardware`

Open Vfx Libraries & Tools
===========================

.. figure:: /_static/images/bkg3.png
   :class: fill

:orange-bold:`Libraries`

.. rst-class:: build

    * :white-bold:`2003 ILM: OpenExr`
    * :white-bold:`2008 ImageEngine: Cortex Vfx`
    * :white-bold:`2008 Sony Image Works: Open Shading Language`
    * :white-bold:`2009 Foundry: OpenFx`
    * :white-bold:`2011 Sony Image Works: Alembic`
    * :white-bold:`2012 Pixar: Open SubDiv`
    * :white-bold:`2012 DreamWorks: OpenVdb`

:orange-bold:`Tools`
--------------------

.. figure:: /_static/images/bkg3.png
   :class: fill

.. rst-class:: build

    * :white-bold:`2008 Disney: PartIO`
    * :white-bold:`2010 Sony Image Works: Open Color IO`
    * :white-bold:`2010 Pixar : USD`
    * :white-bold:`2012 ImageEngine : Gaffer`

.. note::

    due to the company name releasing them:

    * code quality standard
    * no fragmentation
    * cooperation (ish)


:orange-bold:`Others`
---------------------

.. figure:: /_static/images/bkg3.png
   :class: fill

.. rst-class:: build

    * :white-bold:`Open Image IO`
    * :white-bold:`Bullet Physics`
    * :white-bold:`DJV`
    * :white-bold:`Natron`
    * :white-bold:`Krita`
    * :white-bold:`Blender`
    * :white-bold:`Appleseed`
    * :white-bold:`Ffmpeg`
    * :white-bold:`Pftools`
    * :white-bold:`Hugin`


.. note::

    * next: benefits and downsides of open source adoption
    * pftools : HDRI management
    * hugin: lens distortion calculation, image stitch

Benefits
========

.. figure:: /_static/images/bkg4.png
   :class: fill

.. rst-class:: build

    * :white-bold:`Speed`
    * :white-bold:`Alternatives`
    * :white-bold:`Standards`
    * :white-bold:`Code Access`

:orange-bold:`Infrastructure Boost`
-----------------------------------

.. figure:: /_static/images/bkg4.png
   :class: fill

.. rst-class:: build

    * :white-bold:`Better memory usage`
    * :white-bold:`Better network share protocols (NFS!)`
    * :white-bold:`Better file systems (XFS)`
    * :white-bold:`Better OpenGL performances`

:orange-bold:`Alternatives`
===========================

.. figure:: /_static/images/bkg4.png
   :class: fill

.. rst-class:: build

    :white-bold:`Nuke -> Natron`

    .. figure:: /_static/images/software/nuke.jpg
            :class: fill

    .. figure:: /_static/images/software/natron.png
        :class: fill


:white-bold:`Framecycles -> DJv`
-----------------------------

.. figure:: /_static/images/software/framecycler.png
    :class: fill

.. rst-class:: build

    .. figure:: /_static/images/software/djv.jpg
        :class: fill


:white-bold:`Katana -> Gaffer`
-------------------------------

.. figure:: /_static/images/software/katana.jpg
    :class: fill

.. rst-class:: build

    .. figure:: /_static/images/software/gaffer.jpg
        :class: fill

:orange-bold:`Standards`
========================

.. figure:: /_static/images/bkg4.png
   :class: fill

.. rst-class:: build

    * :white-bold:`Alembic`
    * :white-bold:`Open Shading language`
    * :white-bold:`Open ColorIO`
    * :white-bold:`Open Exr`
    * :white-bold:`Python`


:orange-bold:`Code access`
==========================

.. figure:: /_static/images/bkg4.png
   :class: fill

.. rst-class:: build

    * :white-bold:`Early access`
    * :white-bold:`Knownledge`
    * :white-bold:`Customizations`
    * :white-bold:`Support`


:orange-bold:`Downsides`
========================

.. figure:: /_static/images/bkg5.png
   :class: fill


.. rst-class:: build

    * :white-bold:`Jungle of licenses`
    * :white-bold:`Version Matrix Hell`
    * :white-bold:`Investment changes`


:orange-bold:`Licenses`
-----------------------

.. figure:: /_static/images/bkg5.png
   :class: fill


.. rst-class:: build

    * :white-bold:`Too many licenses`
    * :white-bold:`Interoperability between licenses`
    .. figure:: /_static/images/licenses.jpg
        :scale: 80 %
.. note::
    ffmpeg : to redistribute closed you need to take out codecs such as H264


:orange-bold:`Version Matrix Hell`
----------------------------------

.. figure:: /_static/images/bkg5.png
   :class: fill


.. rst-class:: build

    * :white-bold:`Each library has dependencies`
    * :white-bold:`Many libraries use different dependencies`
    * :white-bold:`Compatibility between versions of the same library`
    .. figure:: /_static/images/versions.jpg
        :scale: 80%


.. slide:: VES

    Vfx Platform

    .. figure:: /_static/images/ves.jpg
        :scale: 60%

    :orange-bold:`http://www.vfxplatform.com/`

.. note::

    * Hard to make the same library work on different applications
    * VES Standard platform !!!

:orange-bold:`Investments`
--------------------------

.. figure:: /_static/images/bkg5.png
   :class: fill


.. rst-class:: build

    * :white-bold:`R&D`
    * :white-bold:`IT`
    .. figure:: /_static/images/ROI.png
        :scale: 80 %

.. note::

    * allow to adopt fixes and updates
    * no need to update commercial versions
    * optimize infrastructure
    * ROI = Return Of Investments


:black-bold:`Conculsions and Suggestions`
=========================================


.. figure:: /_static/images/suggestions.jpg
    :class: fill


.. rst-class:: build

    * Stick to the supported platform
    * Follow the standards
    * Make decisions earlier in the process
    * Not just DCC applications !
    * All the informations you need are out there
    * Contribute back !

.. note::

    * Many distro, stick to the standards (Red Hat!)
    * Changes have costs, be careful
    * Software being opensource change quickly
    * Production can move to OpenOffice!
    * Just go and ask, learn and contribute back!



:black-bold:`Demo`
========================

.. figure:: /_static/images/bkg2.png
   :class: fill

    * :orange-bold:`Gaffer` : ImageEngine
    * :orange-bold:`OSL` : Sony Imageworks
    * :orange-bold:`OCIO` : Sony Imageworks
    * :orange-bold:`Alembic` : Sony Imageworks
    * :orange-bold:`Appleseed` : appleseedhq




:black-bold:`References`
========================

.. figure:: /_static/images/bkg2.png
   :class: fill

.. rst-class:: small-font

    * :orange-bold:`VES Reference Platform` : http://www.vfxplatform.com/
    * :orange-bold:`OpenVFX` : http://opensourcevfx.org/
    * :orange-bold:`Disney` : http://www.disneyanimation.com/technology/publications
    * :orange-bold:`Pixar` : http://graphics.pixar.com/
    * :orange-bold:`Sony Imageworks` : http://opensource.imageworks.com/
    * :orange-bold:`Dreamworks` : http://www.openvdb.org/
    * :orange-bold:`ImageEngine`: http://imageengine.github.io/gaffer/
    * :orange-bold:`Appleseed` : http://appleseedhq.net/
    * :orange-bold:`Licensing help` : http://choosealicense.com/licenses/
    * :orange-bold:`Simplified licenses` : https://tldrlegal.com/



QA
==
.. figure:: /_static/images/questions.jpg
    :class: fill

